#!/bin/bash

#Exporting the backend S3 Bucket
bucket_name=$(terraform output S3_Bucket)

#Update the created backend in the deployment pipeline
curl --request PUT --header "Private-Token:${TOKEN}" \
                            "https://gitlab.com/api/v4/projects/34374418/variables/DEV_BUCKET" \
                            --form "value=${bucket_name}"

#Update Dev access key
curl --request PUT --header "Private-Token:${TOKEN}" \
                            "https://gitlab.com/api/v4/projects/34374418/variables/DEV_AWS_ACCESS_KEY" \
                            --form "value=${DEV_AWS_ACCESS_KEY}"

#Update Dev secret key
curl --request PUT --header "Private-Token:${TOKEN}" \
                            "https://gitlab.com/api/v4/projects/34374418/variables/DEV_SECRET_ACCESS_KEY" \
                            --form "value=${DEV_SECRET_ACCESS_KEY}"

#Trigger deployment pipeline
curl --request POST --form token=${DEPLOYMENT_TOKEN} \
                            --form ref=master \
                            https://gitlab.com/api/v4/projects/34374418/trigger/pipeline
